//
//  ViewController.m
//  ClLocation
//
//  Created by click labs 115 on 10/26/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    CLLocationManager * manager;
}
@property (strong, nonatomic) IBOutlet UITextField *txtForLat;
@property (strong, nonatomic) IBOutlet UITextField *txtForLon;
@property (strong, nonatomic) IBOutlet UILabel *lblLat;
@property (strong, nonatomic) IBOutlet UILabel *lblLon;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    
    
    _txtForLat.layer.borderWidth = 1.0f;
    _txtForLat.layer.borderColor = [UIColor blackColor].CGColor;
    _txtForLon.layer.borderWidth = 1.0f;
    _txtForLon.layer.borderColor = [UIColor blackColor].CGColor;
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)getLocation:(id)sender {
    
    [manager startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        _lblLon.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        _lblLat.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
    [manager stopUpdatingLocation];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
